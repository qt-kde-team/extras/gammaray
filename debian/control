Source: gammaray
Priority: optional
Section: devel
Maintainer: Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>
Uploaders: Jakub Adam <jakub.adam@ktknet.cz>
Build-Depends: cmake,
               debhelper-compat (= 13),
               gdb,
               glslang-tools,
               libgraphviz-dev,
               libkf6coreaddons-dev,
               libkf6syntaxhighlighting-dev,
               libwayland-dev (>= 1.12.0) [linux-any],
               qml6-module-qtqml,
               qml6-module-qtqml-workerscript,
               qml6-module-qtquick,
               qml6-module-qtquick-controls,
               qml6-module-qtquick-templates,
               qml6-module-qtquick-window,
               qt6-3d-assimpsceneimport-plugin,
               qt6-3d-defaultgeometryloader-plugin,
               qt6-3d-dev,
               qt6-3d-gltfsceneio-plugin,
               qt6-3d-scene2d-plugin,
               qt6-base-private-dev,
               qt6-connectivity-dev [linux-any],
               qt6-declarative-dev,
               qt6-declarative-dev-tools,
               qt6-declarative-private-dev,
               qt6-location-dev,
               qt6-positioning-dev,
               qt6-shadertools-dev,
               qt6-svg-dev,
               qt6-tools-dev,
               qt6-tools-dev-tools,
               qt6-translations-l10n,
               qt6-wayland [linux-any],
               qt6-wayland-dev [linux-any],
               qt6-webengine-dev [amd64 arm64 armhf i386],
               xauth,
               xvfb
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/gammaray.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/gammaray
Homepage: https://www.kdab.com/development-resources/qt-tools/gammaray/

Package: gammaray
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: qml6-module-qtquick-controls,
         qml6-module-qtquick-scene3d,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: gdb
Description: Tool for examining the internals of Qt application
 GammaRay is a tool for examining the internals of a Qt application and
 to some extent also manipulate it. GammaRay uses injection methods to
 hook into an application at runtime and provide access to a wide variety
 of interesting information. It provides easy ways of navigating through
 the complex internal structures you find in some Qt frameworks, such as
 QGraphicsView, model/view, QTextDocument, state machines and more.

Package: gammaray-plugin-quickinspector
Architecture: any
Depends: gammaray (= ${binary:Version}),
         qml6-module-qtquick,
         qml6-module-qtquick-controls,
         ${misc:Depends},
         ${shlibs:Depends}
Enhances: gammaray
Description: GammaRay plugin for inspecting QtQuick2 applications
 This plugin provides the user with the following inspection and debugging
 facilities:
 .
  * Tree view of all QQuickItems in the scene, marking invisible items
    and items having focus.
  * Object inspector allowing to see all the properties, inspect
    signal-slot connections, and directly invoke slots and Q_INVOKABLE
    methods.
  * Live-preview of the QtQuick scene inside the GammaRay window.
  * Qt Quick scene graph inspector.

Package: gammaray-plugin-waylandinspector
Architecture: linux-any
Depends: gammaray (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Enhances: gammaray
Description: Wayland compositor inspector plugin for GammaRay
 The Wayland compositor inspector allows one to browse QWaylandClient
 instances and their associated resources, as well as observe Wayland
 events.

Package: gammaray-plugin-kjobtracker
Architecture: any
Depends: gammaray (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Enhances: gammaray
Description: KJob tracker plugin for GammaRay
 This plugin can be used to monitor KJob instances within a KDE-based
 application.

Package: gammaray-plugin-bluetooth
Architecture: linux-any
Depends: gammaray (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Enhances: gammaray
Description: QtBluetooth type support for GammaRay
 This plugin adds support for QtBluetooth types into GammaRay.

Package: gammaray-plugin-positioning
Architecture: any
Depends: gammaray (= ${binary:Version}),
         qml6-module-qtlocation,
         qml6-module-qtpositioning,
         qml6-module-qtquick,
         ${misc:Depends},
         ${shlibs:Depends}
Enhances: gammaray
Description: Qt6Positioning type support for GammaRay
 This plugin adds support for Qt6Positioning types into GammaRay.

Package: gammaray-dev
Architecture: any
Section: libdevel
Depends: gammaray (= ${binary:Version}), ${misc:Depends}
Description: GammaRay plugin development files
 GammaRay is a tool for examining the internals of a Qt application and
 to some extent also manipulate it. GammaRay uses injection methods to
 hook into an application at runtime and provide access to a wide variety
 of interesting information. It provides easy ways of navigating through
 the complex internal structures you find in some Qt frameworks, such as
 QGraphicsView, model/view, QTextDocument, state machines and more.
 .
 This package contains header files used for building 3rd party GammaRay
 plugins.
